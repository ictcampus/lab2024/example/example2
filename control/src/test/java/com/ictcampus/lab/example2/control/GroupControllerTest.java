package com.ictcampus.lab.example2.control;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.test.web.servlet.MockMvc;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Set;

/**
 * Units Test for {@link GroupController}
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */

@WebMvcTest()
@Import( GroupController.class )
class GroupControllerTest {
	@Autowired
	private MockMvc mockMvc;

	@SpringBootConfiguration
	@ComponentScan( "com.ictcampus.lab.example2" )
	public static class TestConfig {
	}

	@Test
	@DisplayName( "Genera la risposta 'BadRequest' poiché manca l'input obbligatorio dei nomi dei progetti" )
	void getGroups_badRequest() throws Exception {
		mockMvc.perform( get( "/api/v1/groups/example1" )
						.accept( APPLICATION_JSON )
						.contentType( APPLICATION_JSON ) )
				.andDo( print() )
				.andExpect( status().isBadRequest() );
	}

	@Test()
	@DisplayName( "Calcola i gruppi di lavoro con l'input dei nomi dei progetti" )
	void getGroups() throws Exception {
		int peoplePerGroup = 5;
		mockMvc.perform( get( "/api/v1/groups/example1" )
						.queryParam( "projects", "quiz,product storage,voyager,web_site,???" )
						.accept( APPLICATION_JSON )
						.contentType( APPLICATION_JSON ) )
				.andDo( print() )
				.andExpect( status().isOk() )
				.andExpect( content().contentType( APPLICATION_JSON ) )
				.andExpect( jsonPath( "$" ).isArray() )
				.andExpect( jsonPath( "$.length()" ).value( 5 ) )
				.andExpect( jsonPath( "$[0].project" ).value( "quiz" ) )
				.andExpect( jsonPath( "$[0].people.length()" ).value( peoplePerGroup ) )
				.andExpect( jsonPath( "$[1].project" ).value( "product storage" ) )
				.andExpect( jsonPath( "$[1].people.length()" ).value( peoplePerGroup ) )
				.andExpect( jsonPath( "$[2].project" ).value( "voyager" ) )
				.andExpect( jsonPath( "$[2].people.length()" ).value( peoplePerGroup ) )
				.andExpect( jsonPath( "$[3].project" ).value( "web_site" ) )
				.andExpect( jsonPath( "$[3].people.length()" ).value( peoplePerGroup ) )
				.andExpect( jsonPath( "$[4].project" ).value( "???" ) )
				.andExpect( jsonPath( "$[4].people.length()" ).value( peoplePerGroup ) );
	}

	@Test
	@DisplayName( "Calcola i gruppi di lavoro con l'input dei nomi dei progetti e il numero di persone per ogni gruppo" )
	void getGroups_onlyGroups() throws Exception {
		int peoplePerGroup = 3;
		mockMvc.perform( get( "/api/v1/groups/example1" )
						.queryParam( "projects", "quiz,product storage,voyager,web_site,???" )
						.queryParam( "peoplePerGroup", String.valueOf( peoplePerGroup ) )
						.accept( APPLICATION_JSON )
						.contentType( APPLICATION_JSON ) )
				.andDo( print() )
				.andExpect( status().isOk() )
				.andExpect( content().contentType( APPLICATION_JSON ) )
				.andExpect( jsonPath( "$.length()" ).value( 9 ) )
				.andExpect( jsonPath( "$[0].project" ).value( "quiz" ) )
				.andExpect( jsonPath( "$[0].people.length()" ).value( peoplePerGroup ) )
				.andExpect( jsonPath( "$[1].project" ).value( "product storage" ) )
				.andExpect( jsonPath( "$[1].people.length()" ).value( peoplePerGroup ) )
				.andExpect( jsonPath( "$[2].project" ).value( "voyager" ) )
				.andExpect( jsonPath( "$[2].people.length()" ).value( peoplePerGroup ) )
				.andExpect( jsonPath( "$[3].project" ).value( "web_site" ) )
				.andExpect( jsonPath( "$[3].people.length()" ).value( peoplePerGroup ) )
				.andExpect( jsonPath( "$[4].project" ).value( "???" ) )
				.andExpect( jsonPath( "$[4].people.length()" ).value( peoplePerGroup ) )
				.andExpect( jsonPath( "$[5].project" ).value( "noProjectName" ) )
				.andExpect( jsonPath( "$[5].people.length()" ).value( peoplePerGroup ) )
				.andExpect( jsonPath( "$[6].people.length()" ).value( peoplePerGroup ) )
				.andExpect( jsonPath( "$[7].people.length()" ).value( peoplePerGroup ) )
				.andExpect( jsonPath( "$[8].people.length()" ).value( 1 ) );
	}

	@Test
	@DisplayName( "Calcola i gruppi di lavoro con l'input dei nomi dei progetti e il numero di persone totali da dividere" )
	void getGroups_onpyMaxPeople() throws Exception {
		int peoplePerGroup = 5;
		mockMvc.perform( get( "/api/v1/groups/example1" )
						.queryParam( "projects", "quiz,product storage,voyager,web_site,???" )
						.queryParam( "people", "7" )
						.accept( APPLICATION_JSON )
						.contentType( APPLICATION_JSON ) )
				.andDo( print() )
				.andExpect( status().isOk() )
				.andExpect( content().contentType( APPLICATION_JSON ) )
				.andExpect( jsonPath( "$" ).isArray() )
				.andExpect( jsonPath( "$.length()" ).value( 2 ) )
				.andExpect( jsonPath( "$[0].project" ).value( "quiz" ) )
				.andExpect( jsonPath( "$[0].people.length()" ).value( peoplePerGroup ) )
				.andExpect( jsonPath( "$[1].project" ).value( "product storage" ) )
				.andExpect( jsonPath( "$[1].people.length()" ).value( 2 ) );
	}

	@Test
	@DisplayName( "Estrazione numeri" )
	void extractNumberTest() throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {
		int number = (int) methodExtractNumber().invoke( null, 5 );
		assertTrue( Set.of( 0, 1, 2, 3, 4 ).contains( number ) );
		assertNotEquals( number, 5 );
	}

	private Method methodExtractNumber() throws NoSuchMethodException {
		Method method = GroupController.class.getDeclaredMethod( "extractNumber", int.class );
		method.setAccessible( true );
		return method;
	}
}