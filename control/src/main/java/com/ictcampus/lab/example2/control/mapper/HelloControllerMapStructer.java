package com.ictcampus.lab.example2.control.mapper;

import com.ictcampus.lab.example2.control.model.WorldSimple;
import com.ictcampus.lab.example2.service.model.World;
import org.mapstruct.Mapper;

/**
 * TODO Add Class Description
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */

@Mapper()
public interface HelloControllerMapStructer {
	WorldSimple toWorld( World source);
}
