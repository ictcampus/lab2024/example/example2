package com.ictcampus.lab.example2.control;

import com.github.javafaker.Faker;
import com.ictcampus.lab.example2.control.mapper.HelloControllerMapStructer;
import com.ictcampus.lab.example2.control.model.Group;
import com.ictcampus.lab.example2.service.HelloService;
import jakarta.websocket.server.PathParam;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * TODO Add Class Description
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */

@RestController
@RequestMapping( "/api/v1/groups" )
@AllArgsConstructor
public class GroupController {
	private HelloControllerMapStructer mapper;
	private HelloService helloService;

	@GetMapping( value = "/generateIO", produces = { MediaType.APPLICATION_JSON_VALUE } )
	public List<Group> getGroups(
			@RequestParam( required = true ) final String[] projects,
			@RequestParam( required = false, defaultValue = "5" ) final Integer peoplePerGroup,
			@RequestParam( required = false, defaultValue = "25" ) final Integer people
	) {
		return generateGroups( projects, peoplePerGroup, people );
	}

	// HTTP GET http://localhost:8080/api/v1/groups/generateIO/33
	@GetMapping( value = "/generateIO/{id}", produces = { MediaType.APPLICATION_JSON_VALUE } )
	public Group getGroup(
			@PathParam( value="id"  ) final String id
	) {
		return Group.builder()
				.name( id )
				.project( id )
				.build();
	}





	private List<Group> generateGroups( final String[] projects, final int peoplePerGroup, final int maxPeople ) {

		Map<Integer, Set<Integer>> partitions = partitionPeople( peoplePerGroup, maxPeople );

		List<Group> groups = new ArrayList<>();
		partitions.forEach( ( key, value ) -> groups.add( Group.builder()
				.name( Faker.instance().team().name() )
				.project( getProject( projects, key ) )
				.people( value )
				.build() ) );

		return groups;
	}

	private Map<Integer, Set<Integer>> partitionPeople( final int peoplePerGroup, final int maxPeople ) {
		final int maxGroups = Math.ceilDiv( maxPeople, peoplePerGroup );

		// map<groupIdx, list of people>
		Map<Integer, Set<Integer>> groups = new HashMap<>();

		for ( int i = 0; i < maxGroups; i++ ) {
			groups.putIfAbsent( i, new HashSet<>() );

			int currentPeoplePerGroup = peoplePerGroup;
			if ( maxGroups - 1 <= i && (maxPeople % peoplePerGroup > 0) ) {
				currentPeoplePerGroup = maxPeople % peoplePerGroup;
			}

			for ( int j = 0; j < currentPeoplePerGroup; j++ ) {
				groups.get( i ).add( extractPerson( groups, maxPeople ) );
			}
		}

		return groups;
	}

	private Map<Integer, Set<Integer>> partitionPeople2( final int peoplePerGroup, final int maxPeople ) {
		final int maxGroups = Math.ceilDiv( maxPeople, peoplePerGroup );

		// map<groupIdx, list of people>
		Map<Integer, Set<Integer>> groups = new HashMap<>();

		for ( int i = 0; i < maxGroups; i++ ) {
			if ( !groups.containsKey( i ) ) {
				groups.put( i, new HashSet<>() );
			}

			for ( int j = 0; j < peoplePerGroup; j++ ) {
				Set<Integer> people = groups.get( i );
				people.add( extractPerson( groups, maxPeople ) );
				groups.replace( i, people );
			}
		}

		return groups;
	}

	private String getProject( final String[] projects, final int index ) {
		if ( projects == null || projects.length <= 0 || index > projects.length - 1 ) {
			return "noProjectName";
		}

		return projects[index];
	}

	private int extractPerson( final Map<Integer, Set<Integer>> groups, final int maxPeople ) {
		int currentPerson = extractNumber( maxPeople );
		Set<Integer> filteredGroups = groups.entrySet().stream().filter( entry -> entry.getValue().contains( currentPerson ) ).map( Map.Entry::getKey )
				.collect( Collectors.toSet() );
		if ( !filteredGroups.isEmpty() ) {
			return extractPerson( groups, maxPeople );
		}

		return currentPerson;
	}

	private static int extractNumber( final int maxNumber ) {
		// Instance of SecureRandom class
		SecureRandom rand = new SecureRandom();
		// Setting the upper bound to generate
		// the random numbers between the specific range
		int upperbound = maxNumber;
		// Generating random numbers from 0 - 999
		// using nextInt()
		return rand.nextInt( upperbound );
	}
}
