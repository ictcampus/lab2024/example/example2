package com.ictcampus.lab.example2.control;

import com.ictcampus.lab.example2.control.mapper.HelloControllerMapStructer;
import com.ictcampus.lab.example2.control.model.WorldSimple;
import com.ictcampus.lab.example2.service.HelloService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * TODO Add Class Description
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */

@RestController
@RequestMapping( "/api/v1/hello" )
@AllArgsConstructor
public class HelloController {
	private HelloControllerMapStructer mapper;
	private HelloService helloService;

	@GetMapping( value = "", produces = { MediaType.APPLICATION_JSON_VALUE } )
	public WorldSimple helloWorld() {
		return mapper.toWorld( helloService.getWorld() );
	}
}
