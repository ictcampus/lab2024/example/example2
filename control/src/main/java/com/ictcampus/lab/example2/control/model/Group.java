package com.ictcampus.lab.example2.control.model;

import lombok.Builder;
import lombok.Singular;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

import java.util.Set;

/**
 * TODO Add Class Description
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */
@Value
@Builder
@Jacksonized
public class Group {
	String name;

	@Singular("person")
	Set<Integer> people;
	String project;
}
