package com.ictcampus.lab.example2.control;

import com.github.javafaker.Faker;
import com.ictcampus.lab.example2.control.mapper.HelloControllerMapStructer;
import com.ictcampus.lab.example2.control.model.Group;
import com.ictcampus.lab.example2.service.HelloService;
import lombok.AllArgsConstructor;
import org.apache.commons.collections4.ListUtils;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * TODO Add Class Description
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */

@RestController
@RequestMapping( "/api/v1/groups" )
@AllArgsConstructor
public class Group3Controller {
	private HelloControllerMapStructer mapper;
	private HelloService helloService;

	@GetMapping( value = "/example3", produces = { MediaType.APPLICATION_JSON_VALUE } )
	public List<Group> getGroups(
			@RequestParam( required = true ) final String[] projects,
			@RequestParam( required = false, defaultValue = "5" ) final Integer peoplePerGroup,
			@RequestParam( required = false, defaultValue = "25" ) final Integer people
	) {
		return generateGroups( projects, peoplePerGroup, people );
	}

	private List<Group> generateGroups( final String[] projects, final int peoplePerGroup, final int people ) {

		List<Integer> numbers = intStreamRange( 1, people );
		Collections.shuffle( numbers, new Random() );

		List<List<Integer>> partitions = ListUtils.partition( numbers, peoplePerGroup );

		List<Group> groups = new ArrayList<>();
		for ( int i = 0; i < partitions.size(); i++ ) {
			List<Integer> peopleInGroup = partitions.get( i );

			groups.add( Group.builder()
					.name( Faker.instance().team().name() )
					.project( getProject(projects, i) )
					.people( peopleInGroup )
					.build() );
		}

		return groups;
	}

	private String getProject( final String[] projects, final int index ) {
		if ( projects == null || projects.length <= 0 || index > projects.length-1 ) {
			return "noProjectName";
		}

		return projects[index];
	}

	private List<Integer> intStreamRange( final int begin, final int end ) {
		List<Integer> numbers = IntStream.rangeClosed( begin, end )
				.boxed()
				.collect( Collectors.toList() );

		return numbers;
	}

	private List<Integer> streamIterate( final int begin, final int end ) {
		List<Integer> numbers = Stream.iterate( begin, n -> n + 1 )
				.limit( end )
				.collect( Collectors.toList() );

		return numbers;
	}
}
