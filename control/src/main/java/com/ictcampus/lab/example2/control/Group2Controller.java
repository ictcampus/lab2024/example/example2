package com.ictcampus.lab.example2.control;

import com.github.javafaker.Faker;
import com.ictcampus.lab.example2.control.mapper.HelloControllerMapStructer;
import com.ictcampus.lab.example2.control.model.Group;
import com.ictcampus.lab.example2.service.HelloService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * TODO Add Class Description
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */

@RestController
@RequestMapping( "/api/v1/groups" )
@AllArgsConstructor
public class Group2Controller {
	private HelloControllerMapStructer mapper;
	private HelloService helloService;

	@GetMapping( value = "/example2", produces = { MediaType.APPLICATION_JSON_VALUE } )
	public List<Group> getGroups(
			@RequestParam( required = true ) final String[] projects,
			@RequestParam( required = false, defaultValue = "5" ) final Integer peoplePerGroup,
			@RequestParam( required = false, defaultValue = "25" ) final Integer people
	) {
		return generateGroups( projects, peoplePerGroup, people );
	}

	private List<Group> generateGroups( final String[] projects, final int peoplePerGroup, final int maxPeople ) {
		Random rand = new Random();
		List<Group> groups = new ArrayList<>();

		final int maxGroups = Math.ceilDiv( maxPeople, peoplePerGroup );

		List<Integer> peopleList = intStreamRange( 1, maxPeople );

		for ( int i = 0; i < maxGroups; i++ ) {
			int currentPeoplePerGroup = peoplePerGroup;
			if ( maxGroups - 1 <= i && (maxPeople % peoplePerGroup > 0) ) {
				currentPeoplePerGroup = maxPeople % peoplePerGroup;
			}

			List<Integer> currentPeopleForGroup = new ArrayList<>();
			for ( int j = 0; j < currentPeoplePerGroup; j++ ) {
				int randomIndex = rand.nextInt( peopleList.size() );
				currentPeopleForGroup.add( peopleList.get( randomIndex ) );
				peopleList.remove( randomIndex );
			}

			groups.add( Group.builder()
					.name( Faker.instance().team().name() )
					.project( getProject( projects, i ) )
					.people( currentPeopleForGroup )
					.build() );
		}

		return groups;
	}

	private List<Integer> intStreamRange( final int begin, final int end ) {
		List<Integer> numbers = IntStream.rangeClosed( begin, end )
				.boxed()
				.collect( Collectors.toList() );

		return numbers;
	}

	private String getProject( final String[] projects, final int index ) {
		if ( projects == null || projects.length <= 0 || index > projects.length - 1 ) {
			return "noProjectName";
		}

		return projects[index];
	}
}
