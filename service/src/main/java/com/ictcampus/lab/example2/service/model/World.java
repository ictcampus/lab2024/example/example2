package com.ictcampus.lab.example2.service.model;

import lombok.Builder;
import lombok.Value;

/**
 * TODO Add Class Description
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */

@Value
@Builder
public class World {
	String name;
	String system;
}
