package com.ictcampus.lab.example2.service;

import com.ictcampus.lab.example2.service.model.World;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * TODO Add Class Description
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */

@Service
@AllArgsConstructor
public class HelloService {
	public World getWorld() {
		return World.builder()
				.name( "Earth" )
				.system( "Solar System" )
				.build();
	}
}
