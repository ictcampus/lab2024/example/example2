# example2

## URL per test
### Mostra un messaggio di benvenuto
* http://localhost:8080/example2/api/v1/hello

### Calcola la divisione in gruppi versione 1
* http://localhost:8080/example2/api/v1/groups/example1?projects=quiz,product storage,voyager,web_site,???
* http://localhost:8080/example2/api/v1/groups/example1?projects=quiz,product storage,voyager,web_site,???&maxPeople=25
* http://localhost:8080/example2/api/v1/groups/example1?projects=quiz,product storage,voyager,web_site,???&groups=5
* http://localhost:8080/example2/api/v1/groups/example1?projects=quiz,product storage,voyager,web_site,???&maxPeople=25&groups=5

### Calcola la divisione in gruppi versione 2
* http://localhost:8080/example2/api/v1/groups/example2?projects=quiz,product storage,voyager,web_site,???
* http://localhost:8080/example2/api/v1/groups/example2?projects=quiz,product storage,voyager,web_site,???&maxPeople=25
* http://localhost:8080/example2/api/v1/groups/example2?projects=quiz,product storage,voyager,web_site,???&groups=5
* http://localhost:8080/example2/api/v1/groups/example2?projects=quiz,product storage,voyager,web_site,???&maxPeople=25&groups=5

### Calcola la divisione in gruppi versione 3
* http://localhost:8080/example2/api/v1/groups/example3?projects=quiz,product storage,voyager,web_site,???
* http://localhost:8080/example2/api/v1/groups/example3?projects=quiz,product storage,voyager,web_site,???&maxPeople=25
* http://localhost:8080/example2/api/v1/groups/example3?projects=quiz,product storage,voyager,web_site,???&groups=5
* http://localhost:8080/example2/api/v1/groups/example3?projects=quiz,product storage,voyager,web_site,???&maxPeople=25&groups=5

## Compile
mvn -U clean install

## Run
mvn -U clean spring-boot:run